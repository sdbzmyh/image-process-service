### Image Process Service

`Image Process Service`是基于`Sparkjava`和`Thumbnailator`库开发的图片处理应用，内嵌jetty容器，直接运行即可提供图片处理服务。

##### 主要

+ 图片裁剪
+ 图片缩放
+ 图片旋转
+ 图片水印

##### 环境

+ 运行环境 [JRE 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

+ 编译环境 [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

+ 工具 [Maven 3.0+](http://maven.apache.org/ )

##### 构建

```java
mvn clean package
```

输出到工程目录`target/image-process-service-${version}-release.zip`包

##### 配置

+ 参见默认配置文件[setting.properties](http://git.oschina.net/secondriver/image-process-service/raw/master/src/main/resources/setting.properties)

##### 下载

- [image-process-server v0.9.2](https://github.com/broncho/oss/raw/master/soft/image-process-service-0.9.2.zip)

##### 运行

```java
java -jar image-process-service-{version}.jar  ${dir}/setting.properties 
```

##### 接口

+ 地址：`http://ip:port/composition`

+ 方法：`HTTP GET`

+ [参数](http://git.oschina.net/secondriver/image-process-service/blob/master/src/main/java/secondriver/process/handler/Param.java)：下表是处理图片的请求参数信息，有默认值的可以不设置请求参数


| 参数名 | 描述 | 取值类型 | 是否要求 | 默认值 |
| ---- |---- |---- |---- |---- |
| mode | 尺寸设置模式 | size,scale | true | scale |
| modeValue | 尺寸取值 | mode=size时 modeValue=width_height, mode=scale时 modeValue=widthScale_heightScale | true | 原图片最大宽高或者不做缩放(宽高缩放比1.0，即1.0_1.0) |
| source | 原图片 | 参见:附录2 | true | 无 |
| cropPosition | mode=size时可用， 图片裁剪位置 | 参见:附录1 | false | center |
| keepAspectRatio| mode=size时可以，图片设置宽高是否保存原比例 | true,false | false | false |
| rotateAngle | 图片旋转角度，负值为逆时针，正值为顺时针 | 整数，小数 | false | 0 |
| quality | 图片质量 | [0.0,1.0] | false | 1.0 |
| watermark | 水印图片 | 参见:附录2 | false | 无 |
| watermarkPosition | 水印图片位置 | 参见:附录1  | false | center |
| watermarkOpacity  | 水印图片透明度 | [0.0, 1.0]  | true | 0.8 |
| outputType  | 输出类型 | 参见：附录3  | true | stream |

##### 示例

+ 源图片地址:

    `http://git.oschina.net/secondriver/image-process-service/raw/master/example/source.png`

    `http%3a%2f%2fgit.oschina.net%2fsecondriver%2fimage-process-service%2fraw%2fmaster%2fexample%2fsource.png`

+ 水印图片地址：

    `http://git.oschina.net/secondriver/image-process-service/raw/master/example/watermark.jpg`

    `http%3a%2f%2fgit.oschina.net%2fsecondriver%2fimage-process-service%2fraw%2fmaster%2fexample%2fwatermark.jpg`
    
+ 案例1: 

```
http://localhost:4567/composition?
watermark=http%3a%2f%2fgit.oschina.net%2fsecondriver%2fimage-process-service%2fraw%2fmaster%2fexample%2fwatermark.jpg
&source=http%3a%2f%2fgit.oschina.net%2fsecondriver%2fimage-process-service%2fraw%2fmaster%2fexample%2fsource.png
```
  ![案例1](http://git.oschina.net/secondriver/image-process-service/raw/master/example/example1.png)

+ 案例2：

```
http://localhost:4567/composition?
mode=size&modeValue=480_480
&watermark=http%3a%2f%2fgit.oschina.net%2fsecondriver%2fimage-process-service%2fraw%2fmaster%2fexample%2fwatermark.jpg
&source=http%3a%2f%2fgit.oschina.net%2fsecondriver%2fimage-process-service%2fraw%2fmaster%2fexample%2fsource.png
&watermarkOpacity=0.3
&watermarkPosition=bottom_right
```
  ![案例2](http://git.oschina.net/secondriver/image-process-service/raw/master/example/example2.png)

##### 附录

+ 位置信息(忽略大小写)

    - BOTTOM_CENTER
    - BOTTOM_LEFT
    - BOTTOM_RIGHT
    - CENTER
    - CENTER_LEFT
    - CENTER_RIGHT
    - TOP_CENTER
    - TOP_LEFT
    - TOP_RIGHT

+ 源信息

    - url（需要通过utf-8进行url encoder） 
    - 文件与服务的静态文件目录的文件相对路径

+ 输出类型(忽略大小写)

    - Stream:流输出;
    - URL: 图片Url地址重定向;
    - Base64:图片内容通过utf-8,Base64编码之后输出：data:image/gif;base64,xxx==;

##### 鸣谢

- [七牛存储提供下载托管](https://portal.qiniu.com/signup?code=3l9a05gqulu8i)
- [开源中国代码托管](http://git.oschina.net/signup?inviter=secondriver)