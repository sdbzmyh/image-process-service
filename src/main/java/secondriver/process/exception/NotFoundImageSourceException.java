package secondriver.process.exception;

/**
 * Author : secondriver
 * Date :  2016/1/6
 */
public class NotFoundImageSourceException extends ClientRequestException {

    public NotFoundImageSourceException(String message) {
        super(message);
    }
}
