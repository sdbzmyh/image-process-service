package secondriver.process.exception;

/**
 * Author : secondriver
 * Date :  2016/1/9
 */
public class SettingException extends Exception {

    public SettingException(String message) {
        super(message);
    }
}
