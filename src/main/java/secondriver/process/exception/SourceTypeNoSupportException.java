package secondriver.process.exception;

/**
 * 图片数据源类型不支持异常
 * <p>
 * 目前支持：
 * 1.https/http的URL
 * 2.相对于静态目录的本地文件
 * <p>
 * Author : secondriver
 * Date :  2016/1/7
 */
public class SourceTypeNoSupportException extends ClientRequestException {

    public SourceTypeNoSupportException(String message) {
        super(message);
    }
}
