package secondriver.process.exception;

/**
 * Author : secondriver
 * Date :  2016/1/8
 */
public class ExceptionModel {
    private int statusCode;
    private String exception;

    public ExceptionModel() {
    }

    public ExceptionModel(int statusCode, String exception) {
        this.statusCode = statusCode;
        this.exception = exception;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
