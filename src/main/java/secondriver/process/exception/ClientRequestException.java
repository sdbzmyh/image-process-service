package secondriver.process.exception;

import spark.Request;
import spark.Response;

/**
 * Author : secondriver
 * Date :  2016/1/8
 */
public class ClientRequestException extends BaseExceptionHandler {

    public ClientRequestException() {
    }

    public ClientRequestException(String message) {
        super(message);
    }

    @Override
    public void handle(Exception exception, Request request, Response response) {
        ExceptionModel model = new ExceptionModel(400, exception.getMessage());
        response.status(400);
        String body = "";
        try {
            response.type("application/json;charset=utf-8");
            body = transformer.render(model);
        } catch (Exception e) {
            body = e.getMessage();
        } finally {
            response.body(body);
        }
    }
}