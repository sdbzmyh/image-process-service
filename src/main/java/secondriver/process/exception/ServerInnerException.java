package secondriver.process.exception;

import spark.Request;
import spark.Response;

/**
 * Author : secondriver
 * Date :  2015/12/28
 */
public class ServerInnerException extends BaseExceptionHandler {

    public ServerInnerException() {
    }

    public ServerInnerException(String message) {
        super(message);
    }

    @Override
    public void handle(Exception exception, Request request, Response response) {
        ExceptionModel model = new ExceptionModel(500, exception.getMessage());
        response.status(500);
        String body = "";
        try {
            response.type("application/json;charset=utf-8");
            body = transformer.render(model);
        } catch (Exception e) {
            body = e.getMessage();
        } finally {
            response.body(body);
        }
    }
}