package secondriver.process.exception;

/**
 * 服务外部文件位置不可用异常
 * <p>
 * Author : secondriver
 * Date :  2016/1/7
 */
public class ExternalFileLocationUnusableException extends SettingException {
    public ExternalFileLocationUnusableException(String message) {
        super(message);
    }
}
