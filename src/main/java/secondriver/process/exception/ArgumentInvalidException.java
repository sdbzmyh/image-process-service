package secondriver.process.exception;

/**
 * 图片处理请求参数无效的异常
 * <p>
 * Author : secondriver
 * Date :  2015/12/28
 */
public class ArgumentInvalidException extends ClientRequestException {

    public ArgumentInvalidException(String message) {
        super(message);
    }

}
