package secondriver.process.exception;

import secondriver.process.core.render.JsonTransformer;
import spark.ExceptionHandler;

/**
 * Author : secondriver
 * Date :  2016/1/8
 */
abstract class BaseExceptionHandler extends RuntimeException implements ExceptionHandler {

    protected JsonTransformer transformer = new JsonTransformer();

    public BaseExceptionHandler() {
    }

    public BaseExceptionHandler(String message) {
        super(message);
    }
}


