package secondriver.process.core.source;

import java.awt.image.BufferedImage;

/**
 * Author : secondriver
 * Date :  2016/1/7
 */
public interface Source<T> {

    T convert(String source);

    boolean isSupported(String source);

    BufferedImage fetch(T t);

    boolean isRemoteSource();
}