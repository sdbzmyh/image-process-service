package secondriver.process.core.source;

import java.util.LinkedList;
import java.util.Objects;

/**
 * Author : secondriver
 * Date :  2016/1/7
 */
public final class SourceManager {

    private static final LinkedList<Source> SUPPORTED_SOURCES = new LinkedList<>();

    private SourceManager() {
    }

    public static void addSource(Source source) {
        Objects.requireNonNull(source);
        if (!SUPPORTED_SOURCES.contains(source)) {
            SUPPORTED_SOURCES.addLast(source);
        }
    }

    public static Source getSource(String source) {
        for (Source aSupportedSource : SUPPORTED_SOURCES) {
            if (aSupportedSource.isSupported(source)) {
                return aSupportedSource;
            }
        }
        return null;
    }
}