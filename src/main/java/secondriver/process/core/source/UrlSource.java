package secondriver.process.core.source;

import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;
import secondriver.process.core.ImageProcessUtil;
import secondriver.process.exception.ArgumentInvalidException;
import spark.utils.StringUtils;

/**
 * Author : secondriver
 * Date :  2016/1/7
 */
public class UrlSource implements Source<URL> {

    @Override
    public URL convert(String source) {
        try {
            return new URL(source);
        } catch (MalformedURLException e) {
            throw new ArgumentInvalidException(e.getMessage());
        }
    }

    @Override
    public boolean isSupported(String source) {
        return StringUtils.isNotEmpty(source)
                && (source.startsWith("https://")
                || source.startsWith("http://"));
    }

    @Override
    public BufferedImage fetch(URL url) {
        return ImageProcessUtil.fetch(url);
    }

    @Override
    public boolean isRemoteSource() {
        return true;
    }
}