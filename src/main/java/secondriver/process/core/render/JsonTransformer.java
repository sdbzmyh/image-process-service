package secondriver.process.core.render;

import com.google.gson.Gson;
import spark.ResponseTransformer;

/**
 * Author : secondriver
 * Date :  2016/1/8
 */
public class JsonTransformer implements ResponseTransformer {

    private Gson gson = new Gson();

    @Override
    public String render(Object model) throws Exception {
        return gson.toJson(model);
    }
}
