package secondriver.process.core;

import spark.utils.StringUtils;

/**
 * Author : secondriver
 * Date :  2016/1/5
 */
public enum ImageMimeType {

    PNG("png", "image/png"),
    JPG("jpg", "image/jpeg"),
    JPEG("jpeg", "image/jpeg"),
    BMP("bmp", "image/bmp"),
    GIF("gif", "image/gif");

    private String formatName;
    private String contentType;

    ImageMimeType(String formatName, String contentType) {
        this.formatName = formatName;
        this.contentType = contentType;
    }

    public String getFormatName() {
        return formatName;
    }

    public String getContentType() {
        return contentType;
    }

    public static ImageMimeType parse(String formatName) {
        ImageMimeType mimeType = null;
        if (StringUtils.isNotEmpty(formatName)) {
            for (ImageMimeType t : ImageMimeType.values()) {
                if (t.formatName.equalsIgnoreCase(formatName)) {
                    mimeType = t;
                    break;
                }
            }
        }
        return mimeType;
    }

    public static ImageMimeType parseOrDefault(String formatName) {
        ImageMimeType v = ImageMimeType.parse(formatName);
        if (v == null) {
            v = ImageMimeType.PNG;
        }
        return v;
    }
}