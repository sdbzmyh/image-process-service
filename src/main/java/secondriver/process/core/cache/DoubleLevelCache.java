package secondriver.process.core.cache;

import java.util.Objects;
import secondriver.process.core.ImageEntry;

/**
 * Author : secondriver
 * Date :  2016/1/5
 */
public class DoubleLevelCache implements Cache {

    private Cache primaryCache;
    private Cache secondaryCache;

    public DoubleLevelCache(Cache primaryCache, Cache secondaryCache) {
        Objects.requireNonNull(primaryCache, "Cache object parameter  can't be null.");
        Objects.requireNonNull(secondaryCache, "Cache object parameter can't be null.");
        this.primaryCache = primaryCache;
        this.secondaryCache = secondaryCache;
    }

    @Override
    public ImageEntry get(String key) {
        ImageEntry v = primaryCache.get(key);
        if (v == null) {
            v = secondaryCache.get(key);
            if (v != null) {
                primaryCache.put(key, v);
            }
        }
        return v;
    }

    @Override
    public void put(String key, ImageEntry imageEntry) {
        primaryCache.put(key, imageEntry);
        secondaryCache.put(key, imageEntry);
    }
}