package secondriver.process.core.cache;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import secondriver.process.core.ImageEntry;
import secondriver.process.core.ImageProcessUtil;
import spark.utils.StringUtils;

/**
 * Author : secondriver
 * Date :  2016/1/5
 */
public class DiskCache implements Cache {

    private final String cacheDir;

    public DiskCache(String cacheDir) {
        if (StringUtils.isEmpty(cacheDir)) {
            throw new IllegalArgumentException("CacheDir parameter can't be null or empty.");
        }
        Path path = Paths.get(cacheDir);
        File dir = path.toFile();
        if (!dir.exists() || !dir.isDirectory()) {
            throw new IllegalArgumentException("CacheDir parameter must be a valid directory.");
        }
        this.cacheDir = cacheDir;
    }

    @Override
    public ImageEntry get(String key) {
        ImageEntry imageEntry = ImageEntry.create(key);
        Path path = Paths.get(cacheDir, imageEntry.getIdentity());
        imageEntry.fetch(() -> ImageProcessUtil.fetch(path.toFile()));
        return imageEntry;
    }

    @Override
    public void put(String key, ImageEntry imageEntry) {
        if (Objects.nonNull(imageEntry) && imageEntry.isComplete()) {
            Path path = Paths.get(cacheDir, imageEntry.getIdentity());
            ImageProcessUtil.push(imageEntry.getImage(), imageEntry.getMimeType().getFormatName(), path.toFile());
        }
    }
}