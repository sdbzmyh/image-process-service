package secondriver.process.core.cache;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import secondriver.process.core.ImageEntry;

/**
 * Author : secondriver
 * Date :  2016/1/5
 */
public class MemoryCache implements Cache {

    private static class LRUCache<K, V> extends LinkedHashMap<K, V> {

        private int maxCache;

        public LRUCache(int maxCache) {
            this.maxCache = maxCache;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > maxCache;
        }
    }

    private final LRUCache<String, ImageEntry> innerLruCache;

    public MemoryCache(int maxCache) {
        if (maxCache <= 0) {
            throw new IllegalArgumentException("MemoryCache maxCache must be set more than 0.");
        }
        innerLruCache = new LRUCache<>(maxCache);
    }

    @Override
    public ImageEntry get(String key) {
        synchronized (innerLruCache) {
            ImageEntry v = innerLruCache.get(key);
            if (Objects.nonNull(v) && v.isComplete()) {
                return v;
            }
        }
        return null;
    }

    @Override
    public void put(String key, ImageEntry imageEntry) {
        synchronized (innerLruCache) {
            if (Objects.nonNull(imageEntry) && imageEntry.isComplete()) {
                innerLruCache.put(key, imageEntry);
            }
        }
    }
}