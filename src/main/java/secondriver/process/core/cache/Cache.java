package secondriver.process.core.cache;


import secondriver.process.core.ImageEntry;

/**
 * Author : secondriver
 * Date :  2016/1/5
 */
public interface Cache {


    ImageEntry get(String key);

    void put(String key, ImageEntry imageEntry);

}
